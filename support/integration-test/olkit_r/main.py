import csv
import sys
import time
from argparse import ArgumentParser, FileType, ArgumentTypeError
import random
import matplotlib.pyplot as plt
import numpy as np
from kafka import KafkaProducer, KafkaConsumer
from sklearn.datasets import make_regression
from sklearn.metrics import mean_squared_error, mean_absolute_error, explained_variance_score, r2_score
from sklearn.model_selection import train_test_split

APP_ID = random.randint(0, 100000)

def str2bool(v):
    if isinstance(v, bool):
        return v
    elif v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise ArgumentTypeError('Boolean value expected.')


def init_argparse():
    parser = ArgumentParser(prog='olkit-k',
                            description='Integration test for Apache Spark ML regression models capable for online learning using Kafka topic as train/test stream. It generates multiple linearly separable batches of numeric data, splits it into train/test set and sends it through two different topics. After all batches, it reads from the output topic and then performs the model evaluation and data visualization.')

    parser.add_argument('-b', '--bootstrap_servers', nargs='*', default=['localhost:9092'],
                        help='List of Kafka bootstrap servers.')
    parser.add_argument('-trt', '--train_topic',
                        help='Kafka topic used as labeled (train) data stream. (default: "train_data")',
                        default='train_data')
    parser.add_argument('-tet', '--test_topic', help='Kafka topic used as test data stream. (default: "test_data")',
                        default='test_data')
    parser.add_argument('-ot', '--output_topic',
                        help='Kafka topic from which the model prediction will be read. (default: "output")',
                        default='output')
    parser.add_argument("-o", "--output_file", type=FileType("w"),
                        help="Output file, default: stdout.", default=sys.stdout)
    parser.add_argument('-ns', '--num_samples', type=int, default=100,
                        help='Number of samples per batch which will later be split into train and test set.')
    parser.add_argument('-tt', '--train_test', type=float, default=0.77, help='Train/test data ratio')
    parser.add_argument('-nf', '--num_features', type=int,
                        help='Number of features which will be generated in the sample. (default: 1)', default=1)
    parser.add_argument('-nif', '--num_redundant', type=int,
                        help='Number of redundant features. These features are generated as random linear combinations of the informative features. (default: 0)',
                        default=0)
    parser.add_argument('-nit', '--num_iterations', type=int,
                        help='Number of iterations/batches after which the model evaluation will be performed. (default: 10)',
                        default=10)

    parser.add_argument('-ai', '--anomalous_iterations', nargs='*', default=[],
                        help='List of numbers which mark iteration as anomalous (to mimic the concept drift). If the iteration is anomalous, samples will be shifted in the features hyperspace by the random value from range [-magnitude,+magnitude].')
    parser.add_argument('-n', '--noise', type=float, default=0.0,
                        help='The standard deviation of the gaussian noise applied to the output.')
    parser.add_argument('-ct', '--consumer_timeout_ms', type=int, default=15000,
                        help='Number of millis to wait after each record consumed over output topic. (default: 15 sec)')
    parser.add_argument('-r', '--random_state', type=int, default=13, help='Random state')
    parser.add_argument('-v', '--visualize', type=str2bool, default=False, help='Visualize datasets')
    parser.add_argument('-sv', '--save_visualizations', type=str2bool, default=False, help='Save visualizations as png.')

    return parser


def generate_data(args, num_samples, random_state):
    return make_regression(n_samples=num_samples, n_features=args.num_features, noise=args.noise,
                           n_informative=(args.num_features - args.num_redundant),
                           random_state=random_state)


def format_labeled_point(X, y):
    return f'({y},[{",".join((str(x) for x in X))}])'


def set_grid(ax, xx, yy):
    ax.set_xlim(xx.min(), xx.max())
    ax.set_ylim(yy.min(), yy.max())
    ax.set_xticks(())
    ax.set_yticks(())


def visualize_data(X, y, X_train, y_train, X_test, y_test, y_predicted, iteration, is_anomalous):
    if is_anomalous:
        an_lab = "(anomalous) "
    else:
        an_lab = ""

    v_train = plt.subplot(1, 2, 1)
    v_test = plt.subplot(1, 2, 2)

    # Plot the training points
    v_train.plot(X_train, y_train)
    v_train.set_title(f"Train data {an_lab}- iteration {iteration}")

    # Plot the testing points
    v_test.plot(X_test, y_test, color='blue')
    v_test.plot(X_test, y_predicted, color='red')
    v_test.set_title(f"Test vs. predicted data {an_lab}- iteration {iteration}")


def send_data(X, y, producer, topic, iteration):
    index = 0

    for (curr_X, curr_y) in zip(X, y):
        producer.send(topic, key=f"{APP_ID}-{iteration}-{index}", value=format_labeled_point(curr_X, curr_y))
        index += 1

    return index


def receive_data(consumer, data):
    for message in consumer:
        parts = message.key.split("-")

        if int(parts[0]) != APP_ID:
            continue
        
        data[int(parts[1])]['y_predicted'].append((int(parts[2]), float(message.value)))


def create_producer(args):
    kwargs = {
        'bootstrap_servers': args.bootstrap_servers,
        'key_serializer': lambda x: x.encode('utf-8'),
        'value_serializer': lambda x: x.encode('utf-8')
    }

    return KafkaProducer(**kwargs)


def create_consumer(args):
    kwargs = {
        'bootstrap_servers': args.bootstrap_servers,
        'enable_auto_commit': True,
        'group_id': 'olkit-group',
        'key_deserializer': lambda x: x.decode('utf-8'),
        'value_deserializer': lambda x: x.decode('utf-8'),
        'consumer_timeout_ms': args.consumer_timeout_ms
    }

    return KafkaConsumer(args.output_topic, **kwargs)


def main():
    args = init_argparse().parse_args()

    plt.rcParams["figure.figsize"] = (15, 5)

    producer = create_producer(args)
    visualize = args.num_features == 1 and args.visualize

    X_all, y_all = generate_data(args, args.num_iterations * args.num_samples, args.random_state)
    X_all_anomalous, y_all_anomalous = generate_data(args, args.num_iterations * args.num_samples, args.random_state + 13)

    writer = csv.DictWriter(args.output_file,
                            fieldnames=['mean_squared_error',
                                        'mean_absolute_error',
                                        'explained_variance_score',
                                        'r2_score'])
    writer.writeheader()

    data = {}
    for iteration in range(0, args.num_iterations):
        is_anomalous = str(iteration) in args.anomalous_iterations

        if is_anomalous:
            X = X_all_anomalous[(iteration * args.num_samples): ((iteration + 1) * args.num_samples)]
            y = y_all_anomalous[(iteration * args.num_samples): ((iteration + 1) * args.num_samples)]
        else:
            X = X_all[(iteration * args.num_samples): ((iteration + 1) * args.num_samples)]
            y = y_all[(iteration * args.num_samples): ((iteration + 1) * args.num_samples)]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=args.train_test,
                                                            random_state=args.random_state)

        data[iteration] = {}
        data[iteration]['X'] = X
        data[iteration]['y'] = y
        data[iteration]['X_train'] = X_train
        data[iteration]['y_train'] = y_train
        data[iteration]['X_test'] = X_test
        data[iteration]['y_test'] = y_test
        data[iteration]['y_predicted'] = []
        data[iteration]['is_anomalous'] = is_anomalous

        send_data(X_train, y_train, producer, args.train_topic, iteration)
        time.sleep(5)
        send_data(X_test, y_test, producer, args.test_topic, iteration)
        time.sleep(5)

    receive_data(create_consumer(args), data)
    for iteration in range(args.num_iterations):
        X_test_eff = []
        y_test_eff = []
        y_predicted_eff = []

        X = data[iteration]['X']
        y = data[iteration]['y']
        X_train = data[iteration]['X_train']
        y_train = data[iteration]['y_train']
        X_test = data[iteration]['X_test']
        y_test = data[iteration]['y_test']
        y_predicted_tuples = data[iteration]['y_predicted']
        is_anomalous = data[iteration]['is_anomalous']

        for (index, label) in y_predicted_tuples:
            X_test_eff.append(X_test[index])
            y_test_eff.append(y_test[index])
            y_predicted_eff.append(label)

        X_test = np.array(X_test_eff)
        y_test = np.array(y_test_eff)
        y_predicted = np.array(y_predicted_eff)

        score = {
            'mean_squared_error': mean_squared_error(y_true=y_test, y_pred=y_predicted),
            'mean_absolute_error': mean_absolute_error(y_true=y_test, y_pred=y_predicted),
            'explained_variance_score': explained_variance_score(y_true=y_test, y_pred=y_predicted),
            'r2_score': r2_score(y_true=y_test, y_pred=y_predicted)
        }

        writer.writerow(score)

        if visualize:
            visualize_data(X, y, X_train, y_train, X_test, y_test, y_predicted, iteration, is_anomalous)

            if args.save_visualizations:
                plt.savefig(f'{APP_ID}-{iteration}.png', bbox_inches='tight')
            else:
                plt.show()


if __name__ == '__main__':
    main()
