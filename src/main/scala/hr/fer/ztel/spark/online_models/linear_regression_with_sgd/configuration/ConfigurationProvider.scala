package hr.fer.ztel.spark.online_models.linear_regression_with_sgd.configuration

import java.io.{IOException, InputStream}
import java.util
import java.util.{Objects, Properties}

class ConfigurationProvider {

  import ConfigurationFields._

  private val DEFAULTS = new util.HashMap[String, String]()

  DEFAULTS.put(SPARK_APP_NAME, "linear-regression-with-sgd-example")
  DEFAULTS.put(SPARK_MASTER_URL, "local[4]")
  DEFAULTS.put(STREAM_KAFKA_BROKERS, "localhost:9092")
  DEFAULTS.put(STREAM_KAFKA_GROUP_ID, "online-learning")
  DEFAULTS.put(STREAM_KAFKA_TOPICS_TEST_DATA, "input")
  DEFAULTS.put(STREAM_KAFKA_TOPIC_OUTPUT, "output")
  DEFAULTS.put(STREAM_KAFKA_TOPICS_TRAIN_DATA, "labeled")
  DEFAULTS.put(LOGGING_LEVEL, "DEBUG")
  DEFAULTS.put(CHECKPOINT_DIR, "/tmp")
  DEFAULTS.put(SPARK_STREAMING_BATCH_TIME_MILLIS, 1000.toString)
  DEFAULTS.put(SPARK_STREAMING_TIMEOUT_SECONDS, (-1).toString)
  DEFAULTS.put(CONVERGENCE_TOLERATION, 0.001.toString)
  DEFAULTS.put(NUMBER_OF_ITERATIONS, 50.toString)
  DEFAULTS.put(STEP_SIZE, 0.1.toString)
  DEFAULTS.put(REGULARIZATION_PARAMETER, 0.0.toString)
  DEFAULTS.put(MICRO_BATCH_FRACTION, 1.0.toString)

  private val env = new Properties()
  env.putAll(DEFAULTS)

  def load(inputStream: InputStream): Unit = {
    try {
      env.load(inputStream)
    } catch {
      case _: Throwable => println("Using default configurations.")
    }
  }

  def get(fieldName: String): String = {
    Objects.requireNonNull(fieldName)
    if (!env.containsKey(fieldName)) throw new IllegalArgumentException(String.format("Configuration %s doesn't exist", fieldName))

    env.getProperty(fieldName)
  }
}
